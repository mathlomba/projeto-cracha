<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caveiras extends Model {

  protected $table = 'caveiras';

  protected $fillable = array('nome', 'id_cartao');

}
