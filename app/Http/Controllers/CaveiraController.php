<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Request;
use Validator;
use App\Caveiras;

class CaveiraController extends Controller {

  public function lista(){
    $membros = Caveiras::all();
    return view('editar')->with('membros', $membros);
  }

  public function novo(){
    return view ('adicionar_membro');
  }

  public function remove($id) {
    $membro = Caveiras::find($id);
    $membro->delete();
    return redirect('/membros');
  }

  public function adiciona(){

    $nome = Request::input('nome');
    $id_cartao = Request::input('id_cartao');

    $validator = Validator::make(
      ['nome' => $nome],
      ['nome' => 'required'],
      ['id_cartao' => $id_cartao],
      ['id_cartao' => 'required'] //id_cartao tem maximo de caracteres -> required|max:?
    );

    if($validator->fails()) {
      return redirect('/membros/novo');
    }

    Caveiras::create(Request::all());
    return redirect ('/membros')->withInput();
  }

}
