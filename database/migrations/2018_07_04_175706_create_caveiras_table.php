<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaveirasTable extends Migration
{

    public function up()
    {
        Schema::create('caveiras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 255);
            $table->string('id_cartao');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('caveiras');
    }
}
