<!-- formulário para adicionar novos membros -->

@extends ('padrao')

@section ('titulo')
  Adicionar novo membro
@stop

@section ('conteudo')
  <h1>Adicionar Membro</h1>

  <form action="/membros/adiciona" method="post">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
      <label>Nome</label>
      <input name="nome" class="form-control" />
    </div>

    <div class="form-group">
      <label>ID do Cartão</label>
      <input name="id_cartao" class="form-control" />
    </div>

    <button class="btn btn-primary" type="submit">
      Adicionar
    </button>

  </form>

@stop
