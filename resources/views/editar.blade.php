@extends ('padrao')

@section ('titulo')
  Editar membros
@stop

@section ('conteudo')
  <h1>Editar Membros</h1>

  @if(old('nome'))
    <div class="alert alert-success">
      O membro {{ old('nome') }} foi registrado!
    </div>
  @endif

  <a href="/membros/novo"><button type="button" class="btn btn-primary" style="margin:15px 0px 25px 0px;">
    Novo Membro
  </button></a>

  <div>
    <table class="table">

      <tr>
        <th>ID</th>
        <th>Nome</th>
        <th>Id_cartão</th>
      </tr>
    @foreach ($membros as $m)
      <tr>
        <td> {{ $m->id }} </td>
        <td> {{ $m->nome }} </td>
        <td> {{ $m->id_cartao }} </td>
        <td>
          <a href="/membros/remove/{{$m->id}}">
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
          </a>
        </td>
      </tr>
    @endforeach
    </table>
  </div>

@stop
