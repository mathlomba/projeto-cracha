<?php

// Route::get('/', function () {
//     return view('welcome');
// });

// Login
Route::get('/', function(){
  return view('login');
});

// Tabela de Membros
Route::get('/tabela', 'MembroController@pontuacao');

// Plantão Aulas
Route::get('/aulas', 'AulaController@aulas');

// Plantão Férias
Route::get('/ferias', 'FeriasController@ferias');

// Editar Membros
Route::get('/membros', 'CaveiraController@lista');
Route::get('/membros/novo', 'CaveiraController@novo');
Route::get('/membros/remove/{id}', 'CaveiraController@remove');
Route::post('/membros/adiciona', 'CaveiraController@adiciona');
